package ru.romanow.jaxrs.web;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.romanow.jaxrs.model.RestResponse;
import ru.romanow.jaxrs.model.UpdateData;

/**
 * Created by ronin on 29.01.16
 */
@RestController
@RequestMapping("/api/details")
public class ComponentController {

    @RequestMapping(method = RequestMethod.PATCH)
    public RestResponse update(@RequestBody UpdateData updateData) {
        RestResponse response = new RestResponse();
        response.setCode(0L);
        response.setMessage("No Error");
        return response;
    }
}
