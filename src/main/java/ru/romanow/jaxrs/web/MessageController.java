package ru.romanow.jaxrs.web;

import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.romanow.jaxrs.model.HelloResponse;

/**
 * User: romanow
 * Date: 28.01.16
 */
@RestController
@RequestMapping("/api/test")
public class MessageController {

    @ApiOperation(value = "Ping method", produces = MediaType.APPLICATION_JSON_VALUE)
    @RequestMapping(method = RequestMethod.GET)
    public HelloResponse hello() {
        HelloResponse helloResponse = new HelloResponse();
        helloResponse.setMessage("Hello, world");
        return helloResponse;
    }
}
