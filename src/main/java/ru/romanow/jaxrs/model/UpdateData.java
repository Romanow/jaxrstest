package ru.romanow.jaxrs.model;

/**
 * Created by ronin on 29.01.16
 */
public class UpdateData {
    private Long id;
    private String data;
    private String comment;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
