package ru.romanow.jaxrs.model;

/**
 * Created by ronin on 29.01.16
 */
public class HelloResponse {
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
