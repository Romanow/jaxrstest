package ru.romanow.jaxrs.model;

/**
 * Created by ronin on 29.01.16
 */
public class RestResponse {
    private Long code;
    private String message;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
